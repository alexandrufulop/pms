<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id')->unsigned(); //hotel id to which this rooms belongs
            $table->smallInteger('number'); //room number
            $table->smallInteger('floor'); //room floor
            $table->string('type'); //room type - single/double etc
            $table->timestamps();
            /* Relationships with other tables */
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
            //a room belongs to a hotel
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
