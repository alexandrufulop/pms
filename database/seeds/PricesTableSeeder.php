<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i=1;
        $room_types = config('data.room_types');

        $agents = DB::table('agents')->get()->toArray();

        foreach($agents as $agent)
        {
            //setting each agent price by room type for example
            $p1 = rand(10, 30);
            $p2 = $p1*1.5;
            $p3 = $p1*2;
            $p4 = $p1*2.5;

            foreach ($room_types as $cur_room_type)
            {
               //Setting prices by room dimensions
                if($cur_room_type == 'single') $price = $p1;
                elseif($cur_room_type == 'double') $price = $p2;
                elseif($cur_room_type == 'apartment') $price = $p3;
                elseif($cur_room_type == 'suite') $price = $p4;

                DB::table('prices')->insert([
                    'agent_id'  => $agent->id,
                    'type' => $cur_room_type,
                    'price' => $price
                ]);

            }

        }

    }
}
