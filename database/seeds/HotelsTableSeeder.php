<?php

use Illuminate\Database\Seeder;

class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //info - obviously we can use factories for more complex applications

        $hotel_names = array('Continental','Hilton','Marriott','Turist');
        $i=0;

        while($i<count($hotel_names))
        {

            DB::table('hotels')->insert([
                'name' => $hotel_names[$i],
                'address' => str_random(10),
                'description' => str_random(20),
            ]);

            $i++;
        }

    }
}
