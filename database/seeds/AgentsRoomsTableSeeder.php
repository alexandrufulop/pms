<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgentsRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rooms_in_db = DB::table('rooms')->select(['id','type'])->get();
        $agents_in_db = DB::table('agents')->select('id')->get()->toArray();

        //if we have rooms and agents
        if(count($rooms_in_db)>0 && count($agents_in_db)>0)
        {
            $i=1;
            while($i<=3)
            {

                foreach ($rooms_in_db as $room)
                {

                    $agent = array_random($agents_in_db);

                    $data[] =
                            [
                                'agent_id'  => $agent->id,
                                'room_id' => $room->id
                    ];
                } //end foreach loop

                $i++;
            }
            //if we have data
            if($data !=null)
            {
                DB::table('agent_room')->insert($data);
            }

        }
    }
}
