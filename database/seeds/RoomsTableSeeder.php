<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i=1;
        $room_types = array('single','double','suite','apartment');

        while($i<=20)
        {
            $floor = rand(1,15); //any from 1-15 floors
            //we want each room to be related to the floor number
            $number = (int)($floor.'00')+$i;

            //generating random room types
            $rand_keys = array_rand($room_types,1);

            DB::table('rooms')->insert([
                'hotel_id'  => DB::table('hotels')->inRandomOrder()->get()->first()->id, //any hotel id from our hotels
                'number' => $number, //room number generation
                'floor' => $floor,
                'type' => $room_types[$rand_keys],
            ]);

            $i++;
        }
    }
}
