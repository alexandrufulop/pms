<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //running our db seeders in the custom order

        $this->call(HotelsTableSeeder::class); //hotels db seed
        $this->call(RoomsTableSeeder::class); //rooms db seed
        $this->call(AgentsTableSeeder::class); //agents db seed
        $this->call(AgentsRoomsTableSeeder::class); //agent_room db seed
        $this->call(PricesTableSeeder::class); //prices db seed
    }
}
