<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agent_names = array('Dna. Gina','Dna. Ionela','Dna. Maria','Dl. Ion', 'Dl. Sima');

        foreach ($agent_names as $agent)
        {

                //seeding
                DB::table('agents')->insert([
                    'name' => $agent,
                ]);

        } //end foreach
    } //end method run
}
