<?php
/**
 *
 * Project: pms
 * Generated: 20-09-2017 @ 9:13 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */

return [
    'welcome'       => 'Welcome to the PMS web application. <br />',
    'no_hotel_selected'  => 'Please select a hotel first...',
    'no_hotel_avail'          => 'No hotels available!',
    's2_type_hotel' => 'Choose your hotel...',
    'no_rooms_avail' => 'There are no rooms associated with this hotel!',
    'add_cleaner'   => 'Add a cleaner...',
    'no_agents_avail' => 'No agents are available at this time',
    'new_entry_fail'  => 'Failed to add the new entry',
    'hotel_edit_success' => 'Hotel data updated!',
    'hotel_removed'     => 'Hotel removed!',
    'room_edit_success' => 'Room data updated!',
    'room_removed'     => 'Room removed!',
    'hotel_added'       => 'New hotel added!',
    'room_added'    => 'New room added!',
    'agent_added'   => 'New agent added!',
    'agent_removed'     => 'Agent removed!',
    'agent_edit_success'    => 'Agent data updated!',
    //
    'workload' =>
            [
                'low'    => "Your cleaning agents can clean more rooms than those that are currently undertaken. <br />If you are in a low season you may release some of the agents for holidays.",
                'normal'    => "Congratulations! Your staff is well balanced for the current workload!",
                'high'    => "Attention! You need more cleaning agents to cover your work amount. <br />Also you may want to redistribute the staff for more efficiency!",
            ]

];