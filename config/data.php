<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 3:52 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */

//application data see AgentController line 40
return [
'room_types'    => ['single', 'double', 'apartment', 'suite' ],
 'assumptions' =>
        [
            'working_hours' => 8, /* How many working hours */
            'break'         => 1, /* Each agent break per workday hours */
            'work_duration' => /* How much time does each activity take ? */
                [
                    'single' => 1,
                    'double' => 2.5,
                    'apartment' => 3,
                    'suite' => 4
                ]
        ]
];