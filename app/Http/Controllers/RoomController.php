<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Hotel;
use App\Models\Room;
use Illuminate\Foundation\PackageManifest;
use Illuminate\Http\Request;

/**
 * Class RoomController
 * @package App\Http\Controllers
 */
class RoomController extends Controller
{
    private $all_agents; //all cleaning agents available

    public $links; //menu links

    public function __construct()
    {
        $this->links =
            [
                [
                    'title' => 'Add new hotel',
                    'link'  => route('hotels.create'),
                ],
                [
                    'title' => 'Edit hotels',
                    'link'  => route('hotels.index',['list'=>true]),
                    //'id'    => 'ClickEditHotel'
                ],
            ];

        $this->all_agents = Agent::select(['id', 'name as text'])->get(); //all agents needed to feed the select2 data
    }

    /**
     * Display a listing of the resource.
     *
     * @param Hotel $hotel
     * @return $this
     */
    public function index()
    {
        $hotel_id = request()->input('hotel_id');

        //if we have a specific hotel for which we want to list the rooms
        if($hotel_id != null)
        {
            //todo more validation is required
            $hotel = Hotel::where('id', $hotel_id)->get()->first(); //we have a hotel

            //adding more links to the menu
            $this->links = array(
                [
                    'title' => 'Add new room to '.$hotel->name,
                    'link' => route('rooms.create', ['hotel_id' => $hotel->id]),
                ]
            );

            //return print_r($hotel); //chrome bug found // crashes Chrome
            if(isset($hotel->rooms) && count($hotel->rooms)>0)
            {
                //ajax request?
                if (request()->ajax()) {

                    //return ajax result with rooms for this hotel
                    return view('partials._display_rooms')
                        ->with('hotel', $hotel)
                        ->with('all_agents', $this->all_agents)
                        ->with('items', $this->links);

                } //if ajax request

                //todo check low prio
                return view('pages/rooms')
                    ->with('hotel', $hotel)
                    ->with('all_agents', $this->all_agents)
                    ->with('items', $this->links);

            } //if the hotel has rooms


            $response = config('messages.no_rooms_avail');
            $response .= view('partials._menu')->with('items', $this->links);

            return response($response, 200);//no rooms available

        }//end if hotel specified
        else             //we list all rooms from all the hotels
            {

                $hotels = Hotel::get();

                if(count($hotels) > 0)
                {
                    return view('pages/list_all_rooms')
                        ->with('all_agents', $this->all_agents)
                        ->with('hotels', $hotels);
                }

                return redirect()->route('hotels.create');

            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //needs filtering
        $hotel = Hotel::findOrFail($request->hotel_id);

        return view('pages/room/create')
            ->with('hotel',$hotel)
            ->with('all_agents',$this->all_agents);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //add newly created room to db if passes validation
        $arr_room = $this->validate(request(), [
            'number' => 'required|numeric|max:10000|unique:rooms,number|unique:rooms,hotel_id',//todo fix validation multiple entries
            'floor' => 'required|numeric|max:1000',
            'type'  => 'required|string|max:100',//we could add a custom validation to check against our values from config see config->data
            'agents' => 'array|max:200'
        ]);

        //todo for sure there must be a better solution...
        $room = new Room($arr_room); //Note: don't forget about fillables!
        $hotel = Hotel::findOrFail($request->hotel_id);
        $room->hotel()->associate($hotel);
        $room->saveOrFail(); //add record to db
        $room->agents()->sync($request->get('agents')); //associate new agents
        //end find new solutions

        return back()->with('success', config('messages.room_added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Room $room
     * @return $this
     */
    public function edit(Room $room)
    {
        return view('pages/room/edit')
            ->with('room',$room)
            ->with('all_agents',$this->all_agents);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Room $room
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, Room $room)
    {

        if($request->ajax())
        {

        //todo validations in requests files
            $agents_arr = [];
            $response = null;
            $response = $room->agents()->detach(); //remove current agents that clean this room

            if(isset($request->agents))
            {


                foreach($request->agents as $agent)
                {
                    if(isset($agent['id']) && !empty($agent['id']) && Agent::findOrFail($agent['id']))
                    {
                        $agents_arr[]=$agent['id'];
                    }
                }

                //if we have new agents we update those into the tables
                if(count($agents_arr)>0)
                {
                    $response = $room->agents()->sync($agents_arr); //add the new agents
                }

                return response(json_encode($response), 200);

            }
        } //finished ajax request for updating agents
        else
            {
                $this->validate(request(), [
                    'number' => 'required|numeric|max:10000|unique:rooms,id|unique:rooms,hotel_id',
                    'floor' => 'required|numeric|max:1000',
                    'type'  => 'required|string|max:100',//we could add a custom validation to check against our values from config see config->data
                    'agents' => 'array|max:200'
                ]);

                $room->number = $request->get('number');
                $room->floor = $request->get('floor');
                $room->type = $request->get('type');
                //Note: nu este redundant pentru ca pt. editare si adaugare camera nu am implementat solutia cu ajax call
                $room->agents()->sync($request->get('agents')); //array with agents id's
                $room->save();

                return back()->with('success',config('messages.room_edit_success'));
            }

         //return response('Error updating cleaning agents for room '.$room->id, 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Room $room
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Room $room)
    {
        $room->delete(); //Note: on cascade a fost implementat de la inceput in migrations...
        return back()->with('success',config('messages.room_removed'));
    }
}
