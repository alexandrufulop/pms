<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
class HotelController extends Controller
{
    //submenu links
    public $links;
    public function __construct()
    {
        $this->links =
        [
            [
                'title' => 'Add new hotel',
                'link'  => route('hotels.create'),
            ],
            [
                'title' => 'Edit hotels',
                'link'  => route('hotels.index',['list'=>true]),
                //'id'    => 'ClickEditHotel'
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param null $list
     * @return $this
     *
     */
    public function index()
    {
        //get all hotels from DB
        $hotels = Hotel::get();

        return view('pages/hotels')->with(
            [
                'hotels' => $hotels,
                'items' => $this->links,
                'list'  => request()->input('list')
            ]);
    }

    /**
     *
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //AJAX example implementation

        /*if(request()->ajax())
        {
            return view('partials/_add_hotel');
        }*/

        return view('pages/hotel/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     *
     */
    public function store(Request $request)
    {
        //hotel new entry validation
        $hotel = $this->validate(request(), [
            'name' => 'required|string|unique:hotels',
            'address' => 'required|string|max:255',
            'description' => 'required|string|max:500'
        ]);

        $result = Hotel::create($hotel); //create new hotel obj

        if($result != null)
        {
            return back()->with('success', config('messages.hotel_added'));
        }

        return back()->withErrors(config('messages.new_entry_fail'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Hotel $hotel
     * @return $this
     */
    public function edit(Hotel $hotel)
    {
        return view('pages/hotel/edit')->with('hotel', $hotel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Hotel $hotel
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Hotel $hotel)
    {
        //
        $this->validate(request(), [
            'name' => [
                'required',
                'string',
                Rule::unique('hotels')->ignore($hotel->id, 'id')],
            'address' => 'required|string|max:255',
            'description' => 'required|string|max:500'
        ]);

//        $hotel = Hotel::findOrFail($hotel->id);

     //   if($hotel->name !== $hotel->name)
        $hotel->name = $request->get('name'); //can also be used request()
        $hotel->address = $request->get('address');
        $hotel->description = $request->get('description');
        $hotel->save();

        return back()->with('success',config('messages.hotel_edit_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Hotel $hotel
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Hotel $hotel)
    {
       $hotel->delete();
       return back()->with('success',config('messages.hotel_removed'));
    }
}
