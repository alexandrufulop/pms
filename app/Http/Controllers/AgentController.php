<?php

namespace App\Http\Controllers;

use App\Models\Price;
use App\Models\Room;
use App\Models\Agent;
use Illuminate\Http\Request;

class AgentController extends Controller
{

    public $links; //menu links

    public function __construct()
    {
        $this->links =
            [
                [
                    'title' => 'Add new agent',
                    'link'  => route('agents.create'),
                ]
            ];

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //display all agents
        $all_agents = Agent::get();

        if(count($all_agents)>0)
        {

            $data = null;

            //////////////////////////
            // Having a little fun //
            /////////////////////////
            ///
            /* Calculating the cleaning agents workload by the room types they are appointed to */
            /*
             * Assumption:
             *      1 agent cleans 1 single room in 1 hour
             *                     1 double in 2.5 hours
             *                     1 apartment in 3 hours
             *                     1 suite in 4 hours
             *
             *      The maximum time all agents can work on one day is 8 hours
             */

            $assumptions = config('data.assumptions');

            /*
             * $assumptions =
             *   [
             *       'working_hours' => 8, // How many working hours
             *       'break'         => 1, // Each agent break per workday hours
             *       'work_duration' => // How much time does each activity take ?
             *       [
             *           'single' => 1,
             *           'double' => 2.5,
             *           'apartment' => 3,
             *           'suite' => 4
             *      ]
             *   ];
             *
             *
             */

            /* How many rooms of each type do we have? */
            foreach (config('data.room_types') as $type)
            {
                $rooms[$type] = Room::get()->where('type', $type)->count();
            }

            $total_agents_available = count($all_agents);

            /* Calculating time needed to clean all the rooms with all the available agents */
            foreach ($assumptions['work_duration'] as $type => $time)
            {
                $time = $time + $assumptions['break'];
                $time_needed[$type] = ($rooms[$type] * $time)/$total_agents_available;
            }

            /* With current personel this is the time needed to clean all the rooms */
            $total_time_needed = array_sum($time_needed);

            /* Making some predictions ... :) */
            if($total_time_needed >= ($assumptions['working_hours'] * 1.5))
            {
                $prediction = config('messages.workload.high');
            }
            elseif($total_time_needed <= ($assumptions['working_hours']/2))
            {
                $prediction = config('messages.workload.low');
            }
            else
                {
                    $prediction = config('messages.workload.normal');
                }

            /* Calculating each person workload to the total amount of rooms */
            $max_rooms = Room::get()->count();//the maximum number of rooms from all hotel

                foreach ($all_agents as $agent)
                {
                    $workload = 0;
                    //Assumption: each cleaning agent charges a price by the size of the room to be cleaned
                    //average price charged by this agent
                    $avg = $agent->prices->avg('price');
                    if($max_rooms>0) {
                        $workload = round((count($agent->rooms) * 100) / $max_rooms); //in percentage
                    }
                    $data[$agent->id]['name'] = $agent->name; //agent name
                    $data[$agent->id]['avg_price'] = $avg; //agent average price
                    $data[$agent->id]['workload'] = $workload; //how many rooms does (s)he cleans already
                    //se poate face procentual mai fain... ex: Agentul are 90% workload...
                }


            //we sort the agents by the less occupied - less workload
            //Note: pentru a salva resourse pe parte de server putem renunta la sortarea de mai sus sortand datele folosind optiunea din datatables
            //array_multisort($price, SORT_ASC, $data);

            return view('pages/agents')
                ->with('prediction', $prediction)
                ->with('items', $this->links)
                ->with('data', $data);
        }

        return view('pages/agent/create')->withErrors(config('messages.no_agents_avail'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/agent/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Implemented for X-editable - Edit in place agents name
        //just an example
        if($request->ajax())
        {

        $agent_id = $request->get('pk'); //from x-editable
        $agent_name = $request->get('value'); //new name;

        if(!empty($agent_name) && is_numeric($agent_id))
        {
            $agent = Agent::find($agent_id);
            $agent->name = $agent_name;

            if($agent->saveOrFail())
            {
                return response('Updated', 200);
            }
        }
            return response('Failed to update agent record', 400);
        }

        //processing add new agent from normal request

        //validation
        $this->validate(request(), [
            'name' => 'required|string|max:200|unique:agents,name',
            'prices' => 'required|array' //todo better not null?
        ]);

        //agent data
        $data['name'] = $request->get('name');
        $input_prices = $request->get('prices');

        //add new agent to DB
        $agent = Agent::create($data);
        $i = 0;
        foreach (config('data.room_types') as $type)
        {
            if($input_prices[$i] != null)
            {
                $data['agent_id'] = $agent->id;
                $data['type'] = $type;
                $data['price'] = $input_prices[$i];
                $price = new Price($data);
                $price->save();     //todo find a better solution!
            }
            $i++;
        }


        return back()->with('success', config('messages.agent_added'));

     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(Agent $agent)
    {
        return view('pages/agent/edit')
            ->with('agent',$agent);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent)
    {
        //validation
        $this->validate(request(), [
            'name' => 'required|string|max:200',
            'prices' => 'required|array' //todo better
        ]);

        $agent->name = $request->get('name');
        $data = [];
        $data['agent_id'] = $agent->id;
        $input_prices = $request->get('prices');

        //todo find a solution to remove this duplicate code
        $i = 0;
        foreach (config('data.room_types') as $type)
        {
            $data['agent_id'] = $agent->id;
            $data['type'] = $type;
            $data['price'] = $input_prices[$i];
            $price = new Price($data);
            $price->save();     //todo find a better solution!

            $i++;
        }

        $agent->saveOrFail(); //save new agent data

        return back()->with('success',config('messages.agent_edit_success'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {
        $agent->delete(); //Note: on cascade a fost implementat de la inceput in migrations...
        return back()->with('success',config('messages.agent_removed'));
    }
}
