<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $fillable = ['name'];

    /* Setting up relationships */
    /* An Agent can clean multiple rooms - belongs to many rooms */
    public function rooms(){
        return $this->belongsToMany(Room::class);
    }

    /* An agent can have many prices for different service types */
    public function prices(){
        return $this->hasMany(Price::class);
    }
}

//Read: Rooms belong to many Agents
//Agents have many prices
