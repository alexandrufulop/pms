<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['hotel_id','number','floor','type'];

    /* Setting up relationships */
    /* A room belongs to a hotel */
    public function hotel(){
        return $this->belongsTo(Hotel::class); //Note: $room->hotel->name;
    }

    /* A Room can have multiple Cleaning Agents */
    public function agents(){
        return $this->belongsToMany(Agent::class)->withPivot('agent_id', 'room_id');
    }
}

//Read:Agents Belongs to Many Rooms