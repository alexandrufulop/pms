<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    //fields allow to be mass filled
    protected $fillable = ['name','address','description'];

    /* Setting up relationships */
    /* A Hotel has many Rooms */
    public function rooms(){
        return $this->hasMany(Room::class); //Note: $hotel->rooms;
    }

    //todo how do we accomplish $hotel->agents ?
}
