<?php
/**
 *
 * Project: pms
 * Generated: 23-09-2017 @ 1:36 AM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
return [
    'welcome' => 'At this stage the application should comply with the <a target="_blank" href=":spec-url" title="Software specifications">specifications</a>. <br />I have added new features which may be helpful - please have a look @ <a target="_blank" href=":agents-section-url" title="Agents section">agents section</a>.</br> I am pretty sure that there are many things that can be improved and I am looking forward to discover them. <br />Thank you for checking out my work.<br /><br />Have a nice weekend!<br />Alexandru'
];