<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 1:46 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>

<div class="container">
    <h2>Edit Hotel {{ $hotel->name }}</h2><br  />
    <form method="post" action="{{action('HotelController@update', $hotel->id)}}">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{$hotel->name}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="address">Address</label>
                <textarea class="form-control" name="address">{{$hotel->address}}</textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="address">Description</label>
                <textarea class="form-control" name="description">{{$hotel->description}}</textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
            </div>
        </div>
</form>
</div>
