<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 10:33 AM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */

?>
<nav class="menu">
<ul class="nav justify-content-center">
    @foreach($items as $item)

        @php
        isset($item['active']) ? $active = 'active' : $active ='';
        isset($item['id']) ? $id = "id='{$item['id']}'" : $id ='';
        @endphp

        <li class="nav-item">
            <a {!! $id !!} class="nav-link {{ $active }}" href="{{ $item['link'] }}">{{$item['title']}}</a>
        </li>
    @endforeach
</ul>
</nav>
