<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 3:47 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>

<div class="container">
    <h2>Edit Room {{ $room->number }}</h2><br  />
    <form method="post" action="{{action('RoomController@update', $room->id)}}">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="number">Number:</label>
                <input type="text" class="form-control" name="number" value="{{$room->number}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Floor:</label>
                <input type="text" class="form-control" name="floor" value="{{$room->floor}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="type">Type</label>
                <select class="form-control" name="type">
                    @php
                        foreach(config('data.room_types') as $type)
                        {
                            $selected = '';
                            if($type == $room->type)
                            {
                                $selected = 'selected="selected"';
                            }

                            echo  '<option '.$selected.' value="'.$type.'">'.ucfirst($type).'</option>';
                        }
                    @endphp
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="agents">Cleaning agents</label>
                    <select name="agents[]" id="SelectRooms-{{$room->id}}" class="selected" data-placeholder="{{ config('messages.add_cleaner') }}" multiple="multiple" data-url="{{ route('rooms.show',['id' => $room->id]) }}">
                        @if(count($room->agents) > 0)
                            @foreach($room->agents->unique() as $agent)
                                <option selected="selected" value="{{$agent->id}}">{{$agent->name}}</option>
                            @endforeach
                        @endif
                    </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
            </div>
        </div>
</form>
</div>

@push('javascript')
    <script type="application/javascript">

        $(document).ready(function()
        {
            //implementing select2 for cleaning agents
            $('.selected').select2({
                minimumResultsForSearch: -1,
                placeholder: function(){
                    $(this).data('placeholder');
                },
                allowClear: true,
                data: {!! json_encode($all_agents) !!}, //our data for feeding the select2 Cleaners Selection
                width: "80%",
                tags: true,
                tokenSeparators: [','],
            }).on('change', function (e) {

                var data = $(this).select2('data');
                //we need the room id to update the agents data
                var url = $(this).data('url');
                var data_to_send = [];

                $.each(data, function( index, value ) {
                    data_to_send.push({'id':value.id,'name':value.text});
                });

                console.log(data_to_send);

                //console.log(data_to_send);
            });

        });

    </script>
@endpush