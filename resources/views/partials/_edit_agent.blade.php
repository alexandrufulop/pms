<?php
/**
 *
 * Project: pms
 * Generated: 23-09-2017 @ 12:34 AM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>
<div class="container">
    <h2>Edit agent {{ $agent->name }}</h2><br  />
    <form method="post" action="{{action('AgentController@update', $agent->id)}}">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{$agent->name}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">Room charge*</div>
        </div>
        @php
            $preturi = array();
            if(count($agent->prices)>0)
            {
                foreach ($agent->prices as $price)
                {
                    $preturi[$price->type] = $price->price;
                }
            }
        @endphp
        @foreach(config('data.room_types') as $type)
            @php
                $pret ='';
                if(isset($preturi[$type]))
                {
                        $pret = $preturi[$type];
                }
            @endphp
           <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="prices">{{$type}}:</label>
                    <input type="text" class="form-control" name="prices[]" value="{{ $pret }}">
                </div>
            </div>
        @endforeach

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
            </div>
        </div>
    </form>
</div>
