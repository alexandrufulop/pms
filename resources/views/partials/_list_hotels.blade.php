<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 12:48 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>
<div class="container">
    <table id="HotelsListing" class="table table-striped display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Address</th>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($hotels as $hotel)
            <tr>
                <td>{{$hotel['id']}}</td>
                <td>{{$hotel['name']}}</td>
                <td>{{$hotel['address']}}</td>
                <td>{{$hotel['description']}}</td>
                <td><a href="{{action('HotelController@edit', $hotel['id'])}}" class="btn btn-warning">Edit</a></td>
                <td>
                    <form action="{{action('HotelController@destroy', $hotel['id'])}}" method="post">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="delete btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@push('javascript')
    <script type="application/javascript">
    jQuery(document).ready(function() {

    $(".delete").popConfirm({
    title: "Are you sure you want to remove this record?"
    });

    });
    </script>
@endpush