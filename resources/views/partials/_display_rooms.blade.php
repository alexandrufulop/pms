<?php
/**
 *
 * Project: pms
 * Generated: 21-09-2017 @ 12:25 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */

//setting up the hotel rooms needed for listing
if(isset($hotel)) $rooms = $hotel->rooms;

?>

<h1> Rooms available @ {{ $hotel->name }}</h1>
{{--submenu for rooms--}}
@include('partials._menu',$items)

<div class="container">
    <div class="room-listing">
        @if(isset($rooms) && count($rooms)>0)
            <table id="Rooms-{{$hotel->name}}" class="rooms display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Room number</th>
                    <th>Floor</th>
                    <th>Room type</th>
                    <th>Cleaning agents</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($rooms as $room)
                    <tr>
                        <td>{{ $room->number }}</td>
                        <td>{{ $room->floor }}</td>
                        <td>{{ $room->type }}</td>
                        <td>
                            <div class="agents">
                                <select name="agents" id="SelectRooms-{{$hotel->name}}" class="selected" data-placeholder="{{ config('messages.add_cleaner') }}" multiple="multiple" data-url="{{ route('rooms.show',['id' => $room->id]) }}">
                                    @if(count($room->agents) > 0)
                                        @foreach($room->agents->unique() as $agent)
                                            <option selected="selected" value="{{$agent->id}}">{{$agent->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </td>
                        <td><a href="{{action('RoomController@edit', $room->id)}}" class="btn btn-warning">Edit</a></td>
                        <td>
                            <form action="{{action('RoomController@destroy', $room->id)}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="delete btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        @else
            <i>{{ config('messages.no_rooms_avail') }}</i>
        @endif
    </div>

</div>


<script type="application/javascript">

    $(document).ready(function()
    {
        //implementing select2 for cleaning agents
        $('.selected').select2({
            minimumResultsForSearch: -1,
            placeholder: function(){
                $(this).data('placeholder');
            },
            allowClear: true,
            data: {!! json_encode($all_agents) !!}, //our data for feeding the select2 Cleaners Selection
            width: "80%",
            tags: true,
            tokenSeparators: [','],
        }).on('change', function (e) {

            var data = $(this).select2('data');
            //we need the room id to update the agents data
            var url = $(this).data('url');
            var data_to_send = [];

            $.each(data, function( index, value ) {
                data_to_send.push({'id':value.id,'name':value.text});
            });

            console.log(data_to_send);
            $.ajax({
                type: "PUT",
                url: url,
                data: {"agents":data_to_send},
                success:function(response) {

                    console.log(response);

                },error: function (data) {

                    var error_message = data.responseText; //default error message
                    var response = data.responseJSON;

                    alert(error_message);
                    console.log(data, response);
                }
            })
            //console.log(data_to_send);
        });

        //datatables
        $('#Rooms-{{$hotel->name}}').DataTable();

    });

</script>
