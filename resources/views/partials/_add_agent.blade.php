<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 11:31 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>
<div class="container">
    <h2>Add new cleaning agent</h2><br  />
    <form method="post" action="{{route('agents.index')}}">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Name*:</label>
                <input type="text" class="form-control" name="name">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">Room charge*</div>
        </div>
            @foreach(config('data.room_types') as $type)
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="prices">{{$type}}:</label>
                    <input type="text" class="form-control" name="prices[]" value="">
                </div>
            </div>
            @endforeach

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Add agent</button>
            </div>
        </div>
    </form>
</div>

