<?php
/**
 *
 * Project: pms
 * Generated: 21-09-2017 @ 12:45 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>

<h1> Cleaning agents available</h1>
<div class="header">{!! $prediction !!}</div>
{{--submenu for rooms--}}
@include('partials._menu',$items)
<div class="container">
    <div class="agents-listing">
        @if(isset($data) && count($data)>0)
            <table class="agents display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Agent name</th>
                    <th>Agent average price (Lei)</th>
                    <th>Agent workload (%)</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $agent_id => $agent_data)
                    <tr>
                        <td data-pk="{{ $agent_id }}" class="editable">{{ $agent_data['name'] }}</td>
                        <td>{{ $agent_data['avg_price'] }}</td>
                        <td>{{ $agent_data['workload'] }}</td>
                        <td><a href="{{action('AgentController@edit', $agent_id)}}" class="btn btn-warning">Edit</a></td>
                        <td>
                            <form action="{{action('AgentController@destroy', $agent_id)}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="delete btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        @else
            <i>{{ config('messages.no_agents_avail') }}</i>
        @endif
    </div>

    @push('javascript')
        <script type="application/javascript">

            $(document).ready(function() {

                $.fn.editable.defaults.mode = 'inline';
                $('.editable').editable({
                    type: 'text',
                    url: '{{ route('agents.store') }}',
                    title: 'Edit agent'
                });


                //datatables
                $(document).ready(function() {
                    $('.agents').DataTable({
                        "order": [[ 2, "asc" ]] //we can order it on the client side to save server resources
                    } );
                } );
            });

        </script>
@endpush

