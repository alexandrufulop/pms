<?php
/**
 *
 * Project: pms
 * Generated: 21-09-2017 @ 8:47 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
//todo make it better
$selected = '';
if(isset($hotel))
{
    $cur_hotel = $hotel->name;
}

?>
<div class="container">
    <div class="Hotels">
        @if(isset($hotels))
            <select class="hotels" name="hotels" data-placeholder="{{ config('messages.s2_type_hotel') }}">
                <option></option>
                @foreach($hotels as $hotel)
                    @php
                        $url = route('rooms.index',['hotel_id'=> $hotel->id]);
                    @endphp
                    @if(isset($cur_hotel) && $cur_hotel==$hotel->name)
                        <option selected="selected" data-url="{{ $url }}" value="{{$hotel->id}}">{{$hotel->name}}</option>
                        @else
                        <option data-url="{{ $url }}" value="{{$hotel->id}}">{{$hotel->name}}</option>
                    @endif

                @endforeach
            </select>
        @else
            <i>{{ config('messages.no_hotel_avail') }}</i>
        @endif
    </div>
</div>

@push('javascript')
    <script type="application/javascript">

        $(document).ready(function() {

            //implementing select2 for hotel selection
            $('.hotels').select2({
                width: "50%",
                minimumResultsForSearch: -1,
                placeholder: function(){
                    $(this).data('placeholder');
                }
            }).on("select2:select", function (e) {
                var selected = e.params.data;
                if (typeof selected !== "undefined") {
                    //var nume_hotel = selected.text;
                   // var id_hotel = selected.id;
                    var url = $(selected.element).data('url');

                    $.ajax({
                        type: "GET",
                        url: url
                        /*data: {"id":id_hotel}*/,
                        success:function(response) {

                            $("#Ajax").html(response); //update the div with the rooms listing

                                     console.log(response);

                        },error: function (data) {

                            var error_message = data.responseText; //default error message
                            var response = data.responseJSON;

                            alert(error_message);
                            console.log(data, response);
                        }
                    })

                }
            });
        });

    </script>
@endpush
