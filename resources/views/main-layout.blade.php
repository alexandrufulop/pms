<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    {{--Bootstrap css--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    {{--font awesome--}}
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: black;
            font-family: 'Raleway', sans-serif;
            font-weight: 160;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            padding:10px;
            align-items: center;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .error-message{
            color:red;
            text-align: center;
            font-weight: bold;
            text-decoration: none;
        }
        .footer{
            text-align: center;
            font-weight: bold;
        }

        .intro {
            text-align: justify;
            font-size: 1.4em;
            width: 90%;
            margin: 0 auto;
            padding: 20px;

        }

        .nav {
            font-weight: bold;
        }

        .menu {
            padding: 1.5rem;
            margin-right: 0;
            margin-bottom: 0;
            margin-left: 0;
            border-width: .2rem;
            position: relative;
            border: solid #f7f7f9;
            border-width: .2rem 0 0;
        }
        ul {
            list-style-type: none;
        }
    </style>

    {{--Jquery, Boostrap, popup--}}
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

    {{--Ajax loading overlay--}}
    {{--//solutie rapida - script static - in mod normal compilam si folosim versioning pt. tot ce inseamna js si scss/css--}}
    <script src="{{asset('js/loadingoverlay.min.js')}}"></script>

    {{--adding our custom scripts--}}
    @stack('script-head')

    {{--adding extra headers when needed--}}
    @yield('extra-headers')

</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>
                    <a href="{{ route('register') }}">Register</a>
                    @endauth
        </div>
    @endif
    <div class="content">
        <div class="links">
            <a href="{{ url('/') }}" title="Home"><span class="title"> Laravel <?php echo app()::VERSION ?> PMS </span></a>
        </div>

        {{--Code start--}}
        <hr />
        <div class="links">
            <a href="{{ url('/hotels') }}">Hotels</a>
            <a href="{{ url('/rooms') }}">Rooms</a>
            <a href="{{ url('/agents') }}">Agents</a>
        </div>
        <hr />
        {{--Code end--}}
        {{--error messages--}}
        <div class="error-message">@include('messages.errors')</div>
        {{-- Main PMS content --}}
        @yield('content')

    </div>
<hr />
<div class="footer">
    (c) Mr. Fulop | {{ date('Y') }}
</div>
</div>


{{--Javascript loaded when need --}}
@stack('javascript')

{{--Other Javascript scripts loaded when needed --}}
@yield('extra-footer')

<script type="application/javascript">
    jQuery(document).ready(function() {

        /* CRSF TOKENS */
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        /* AJAX LOADER DISPLAY */
        /* Bind loader to ajax request */
        $(document).ajaxStart(function(){
            $.LoadingOverlay("show");
        });

        $(document).ajaxStop(function(){
            $.LoadingOverlay("hide");
        });

     });
</script>
</body>
</html>
