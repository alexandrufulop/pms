<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 3:17 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>

@extends('.main-layout')

@push('script-head')
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endpush

@section('content')

    <div class="title">
        Rooms
    </div>
{{--    @include('partials._hotel_selector')--}}
    @foreach($hotels as $hotel)
        @php
            $items = array(
                        [
                            'title' => 'Add new room to '.$hotel->name,
                            'link' => route('rooms.create', ['hotel_id' => $hotel->id]),
                        ]
                    );
        @endphp

        @include('partials._display_rooms')
    @endforeach



@endsection
