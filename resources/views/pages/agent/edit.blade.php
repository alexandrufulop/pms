<?php
/**
 *
 * Project: pms
 * Generated: 23-09-2017 @ 12:33 AM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>
@extends('.main-layout')

@section('content')
    @include('partials._edit_agent')
@endsection
