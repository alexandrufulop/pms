<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 11:31 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>
@extends('.main-layout')

@section('content')
    @include('partials._add_agent')
@endsection
