<?php
/**
 *
 * Project: pms
 * Generated: 20-09-2017 @ 6:48 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>
@extends('.main-layout')

@push('script-head')
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endpush

@section('content')

    <div class="title">
        Hotels
    </div>

    <div class="hotel-selector">@include('partials._hotel_selector',$hotels)</div>
    {{--submenu--}}
        @include('partials._menu',$items)
    {{--load hotel list for editing--}}
    @if(isset($list))
        @include('partials._list_hotels')
    @endif

    {{--AJAX output--}}
    <div id="Ajax"></div>

@endsection

@push('javascript')
    <script type="application/javascript">

        $(document).ready(function()
        {
            $('#HotelsListing').DataTable();

        });

    </script>
@endpush

