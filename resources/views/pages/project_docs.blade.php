<?php
/**
 *
 * Project: pms
 * Generated: 23-09-2017 @ 1:44 AM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>
@extends('.main-layout')


@section('content')

    <div class="content">
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:21.0pt;font-family:"Trebuchet MS","sans-serif"; mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"; color:black'>
  Property management system</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  The Property Management System (PMS) aims to help Hotel owners in
  managing their property.</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Essential elements:</span>
        </p>
        <p class="MsoNormal" style=
        'margin-top:5.0pt;margin-right:0in;margin-bottom:0in; margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:normal; mso-outline-level:2'>
            <strong><span style=
                          'font-size:13.0pt;font-family:"Trebuchet MS","sans-serif"; mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"; color:black'>
  Hotel</span></strong>
        </p>
        <ul style='margin-top:0in' type="disc">
            <li class="MsoNormal" style=
            'color:black;margin-bottom:0in;margin-bottom:.0001pt; text-align:justify;line-height:normal;mso-list:l3 level1 lfo1;tab-stops: list .5in;vertical-align:baseline'>
  <span style=
        'font-size:12.0pt; font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman"'>
    Name</span>
            </li>
            <li class="MsoNormal" style=
            'color:black;margin-bottom:0in;margin-bottom:.0001pt; text-align:justify;line-height:normal;mso-list:l3 level1 lfo1;tab-stops: list .5in;vertical-align:baseline'>
  <span style=
        'font-size:12.0pt; font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman"'>
    Address</span>
            </li>
            <li class="MsoNormal" style=
            'color:black;margin-bottom:0in;margin-bottom:.0001pt; text-align:justify;line-height:normal;mso-list:l3 level1 lfo1;tab-stops: list .5in;vertical-align:baseline'>
  <span style=
        'font-size:12.0pt; font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman"'>
    Description</span>
            </li>
        </ul>
        <p class="MsoNormal" style=
        'margin-top:5.0pt;margin-right:0in;margin-bottom:0in; margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:normal; mso-outline-level:2'>
            <strong><span style=
                          'font-size:13.0pt;font-family:"Trebuchet MS","sans-serif"; mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"; color:black'>
  Room</span></strong>
        </p>
        <ul style='margin-top:0in' type="disc">
            <li class="MsoNormal" style=
            'color:black;margin-bottom:0in;margin-bottom:.0001pt; text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;tab-stops: list .5in;vertical-align:baseline'>
  <span style=
        'font-size:12.0pt; font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman"'>
    Number</span>
            </li>
            <li class="MsoNormal" style=
            'color:black;margin-bottom:0in;margin-bottom:.0001pt; text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;tab-stops: list .5in;vertical-align:baseline'>
  <span style=
        'font-size:12.0pt; font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman"'>
    Floor</span>
            </li>
            <li class="MsoNormal" style=
            'color:black;margin-bottom:0in;margin-bottom:.0001pt; text-align:justify;line-height:normal;mso-list:l2 level1 lfo2;tab-stops: list .5in;vertical-align:baseline'>
  <span style=
        'font-size:12.0pt; font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman"'>
    Type<span style='mso-tab-count:1'>&nbsp;&nbsp;</span> (single,
    double, etc)</span>
            </li>
        </ul>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-top:5.0pt;margin-right:0in;margin-bottom:0in; margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:normal; mso-outline-level:2'>
            <strong><span style=
                          'font-size:13.0pt;font-family:"Trebuchet MS","sans-serif"; mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"; color:black'>
  Cleaning Agent</span></strong>
        </p>
        <ul style='margin-top:0in' type="disc">
            <li class="MsoNormal" style=
            'color:black;margin-bottom:0in;margin-bottom:.0001pt; text-align:justify;line-height:normal;mso-list:l0 level1 lfo3;tab-stops: list .5in;vertical-align:baseline'>
  <span style=
        'font-size:12.0pt; font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman"'>
    Name</span>
            </li>
            <li class="MsoNormal" style=
            'color:black;margin-bottom:0in;margin-bottom:.0001pt; text-align:justify;line-height:normal;mso-list:l0 level1 lfo3;tab-stops: list .5in;vertical-align:baseline'>
  <span style=
        'font-size:12.0pt; font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman"'>
    Price per room</span>
            </li>
        </ul>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-top:5.0pt;margin-right:0in;margin-bottom:0in; margin-left:0in;margin-bottom:.0001pt;line-height:normal;mso-outline-level: 1'>
<span style=
      'font-size:16.0pt;font-family:"Trebuchet MS","sans-serif"; mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"; color:black;mso-font-kerning:18.0pt'>
  Relations</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  A <strong>Hotel</strong> has many <strong>Rooms</strong>.</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  A <strong>Cleaning Agent</strong> can be responsible for many
  <strong>Rooms</strong>, from different
  <strong>Hotels</strong>.</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  A <strong>Room</strong> can have multiple <strong>Cleaning
  Agents</strong> responsible for it.</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:12.0pt;line-height:normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family: "Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-top:5.0pt;margin-right:0in;margin-bottom:0in; margin-left:0in;margin-bottom:.0001pt;line-height:normal;mso-outline-level: 1'>
<span style=
      'font-size:16.0pt;font-family:"Trebuchet MS","sans-serif"; mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"; color:black;mso-font-kerning:18.0pt'>
  Tasks</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-top:5.0pt;margin-right:0in;margin-bottom:0in; margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:normal; mso-outline-level:2'>
            <strong><span style=
                          'font-size:13.0pt;font-family:"Trebuchet MS","sans-serif"; mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"; color:black'>
  Stage 1</span></strong>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Build CRUD interfaces for every Model. (including relationship
  management)</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Only functionality is important in this stage, so don’t waste
  time on design elements (html or css).</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
            <strong><span style=
                          'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Stage 2</span></strong>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Implement</span> <span style=
                         'font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family: "Times New Roman"'>
  <a href="https://select2.github.io/"><span style=
                                             'font-family: "Arial","sans-serif";color:#1155CC'>select2</span></a></span>
            <span style=
                  'font-size:12.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family: "Times New Roman";color:black'>
  when choosing the <strong>Cleaning Agent</strong> responsible for
  a <strong>Room</strong>.</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-top:5.0pt;margin-right:0in;margin-bottom:0in; margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:normal; mso-outline-level:2'>
            <strong><span style=
                          'font-size:13.0pt;font-family:"Trebuchet MS","sans-serif"; mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"; color:black'>
  Stage 3</span></strong>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  List all <strong>Rooms</strong> using a</span> <span style=
                                                       'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  <a href="https://www.datatables.net/"><span style=
                                              'font-family:"Arial","sans-serif";color:#1155CC'>datatables</span></a></span>
            <span style=
                  'font-size:12.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family: "Times New Roman";color:black'>
  and ajax solution.</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:12.0pt;line-height:normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family: "Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-top:5.0pt;margin-right:0in;margin-bottom:0in; margin-left:0in;margin-bottom:.0001pt;line-height:normal;mso-outline-level: 1'>
<span style=
      'font-size:16.0pt;font-family:"Trebuchet MS","sans-serif"; mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"; color:black;mso-font-kerning:18.0pt'>
  Implementation</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Use the latest version of the</span> <span style=
                                             'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  <a href="https://laravel.com/"><span style=
                                       'font-family:"Arial","sans-serif";color:#1155CC'>Laravel</span></a></span>
            <span style=
                  'font-size:12.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family: "Times New Roman";color:black'>
  framework.</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Use a dedicated</span> <span style=
                               'font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family: "Times New Roman"'>
  <a href="https://about.gitlab.com/"><span style=
                                            'font-family: "Arial","sans-serif";color:#1155CC'>GitLab</span></a></span>
            <span style=
                  'font-size:12.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family: "Times New Roman";color:black'>
  repo and add the following users, with developer rights:</span>
        </p>
        <ul style='margin-top:0in' type="disc">
            <li class="MsoNormal" style=
            'color:black;margin-bottom:0in;margin-bottom:.0001pt; text-align:justify;line-height:normal;mso-list:l1 level1 lfo4;tab-stops: list .5in;vertical-align:baseline'>
  <span style=
        'font-size:12.0pt; font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman"'>
    &lt;&lt;you username&gt;&gt;</span>
            </li>
        </ul>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;line-height: normal'>
  <span style=
        'font-size:12.0pt;font-family:"Times New Roman","serif"; mso-fareast-font-family:"Times New Roman"'>
  &nbsp;</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Start developing with <strong>Stage 1</strong>.</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Inform your supervisor after each stage is finished. Don’t begin
  working on the next stage before getting your supervisor’s
  approval.</span>
        </p>
        <p class="MsoNormal" style=
        'margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal'>
<span style=
      'font-size:12.0pt;font-family:"Arial","sans-serif"; mso-fareast-font-family:"Times New Roman";color:black'>
  Commit and push as often as possible. At least a couple a times a
  day.</span>
        </p>
        <p class="MsoNormal">
            &nbsp;
        </p>
    </div>


@endsection


