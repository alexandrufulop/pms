@extends('.main-layout')

@section('content')

    @if(empty($errors->all()))
     <div class="title">
         Welcome
     </div>
        <div class="container">
            @lang('messages.welcome',['spec-url' => url('docs'), 'agents-section-url' => route('agents.index')])
        </div>
@endif
@endsection

