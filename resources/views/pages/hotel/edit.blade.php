<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 1:45 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>
@extends('.main-layout')

@section('content')
    @include('partials._edit_hotel')
@endsection
