<?php
/**
 *
 * Project: pms
 * Generated: 22-09-2017 @ 12:17 PM
 *
 * User:        Mr. Fulop
 * Created by:  Online Promoters
 * Email:       online@promoters.ro
 * Web:         https://online.promoters.ro/
 */
?>
@extends('.main-layout')

@section('content')
    @include('partials._add_hotel')
@endsection